import React from 'react';
import { Provider } from 'react-redux';
import textConstants from '../../constants/textConstants';
import store from '../../store/store';
import ReferenceBookList from '../referenceBookList';

function App(): React.ReactElement {
  return (
    <Provider store={store}>
      <div className="app">
        <h1>{textConstants.appTitle}</h1>
        <h2>{textConstants.appSubTitle}</h2>
        <div className="app__container">
          <ReferenceBookList />
        </div>
      </div>
    </Provider>
  );
}

export default App;
