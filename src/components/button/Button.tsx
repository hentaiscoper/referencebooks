import React from 'react';

interface ButtonProps {
  className: string;
  type: string;
  icon: React.ReactElement;
}

function Button(props: ButtonProps): React.ReactElement {
  return <button type="button">add</button>;
}

export default Button;
