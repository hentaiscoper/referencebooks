import React from 'react';
import ReferenceBookItemView from './ReferenceBookItemView';
import { DataTypes } from '../../types/dataTypes';

interface Props {
  item: DataTypes.ReferenceBook;
}

function ReferenceBookItem(props: Props): React.ReactElement {
  const { item } = props;

  return <ReferenceBookItemView item={item} />;
}

export default ReferenceBookItem;
