import React from 'react';
import { DataTypes } from '../../types/dataTypes';

interface Props {
  item: DataTypes.ReferenceBook;
}

function ReferenceBookItemView(props: Props): React.ReactElement {
  const { item } = props;

  return <li>{item.title}</li>;
}

export default ReferenceBookItemView;
