import React from 'react';
import ReferenceBookListView from './ReferenceBookListView';
import ReferenceBookListHooks from './ReferenceBookListHooks';

function ReferenceBookList(): React.ReactElement {
  const [fetchStatus, referenceBooks] = ReferenceBookListHooks.useReferenceBooksData();

  function onAddReferenceBookClick() {}

  return <ReferenceBookListView referenceBooks={referenceBooks} />;
}

export default ReferenceBookList;
