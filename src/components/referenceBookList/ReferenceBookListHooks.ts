import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { Constants } from '../../types/constants';
import { DataTypes } from '../../types/dataTypes';
import { Redux } from '../../types/store/redux';
import ReferenceBooksActions from '../../store/actions/referenceBooksActions';

export default class ReferenceBookListHooks {
  static useReferenceBooksData(): [Constants.FetchConstantsKey, DataTypes.ReferenceBook[]] {
    const dispatch = useDispatch();
    const fetchStatus = useSelector((state: Redux.RootState) => state.referenceBooks.fetchStatus);
    const data = useSelector((state: Redux.RootState) => state.referenceBooks.data);

    useEffect(() => {
      dispatch(ReferenceBooksActions.getReferenceBooks());
    }, [dispatch]);

    return [fetchStatus, data];
  }
}
