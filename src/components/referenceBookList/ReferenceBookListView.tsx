import React from 'react';
import { ReferenceBooks } from '../../types/store/referenceBooks';
import ReferenceBookItem from '../referenceBookItem';

interface ReferenceBookListViewProps {
  referenceBooks: ReferenceBooks.Data;
}

function ReferenceBookListView(props: ReferenceBookListViewProps): React.ReactElement {
  const { referenceBooks } = props;

  return (
    <ul>
      {referenceBooks.map((item) => (
        <ReferenceBookItem key={item.id} item={item} />
      ))}
    </ul>
  );
}

export default ReferenceBookListView;
