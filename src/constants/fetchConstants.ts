const FetchConstants = {
  PENDING: 'PENDING',
  SUCCESS: 'SUCCESS',
  FAILURE: 'FAILURE',
  WAITING: 'WAITING',
};

export default FetchConstants;
