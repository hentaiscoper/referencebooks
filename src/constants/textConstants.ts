const textConstants = {
  appTitle: 'Тестовое задание ТАП',
  appSubTitle: 'Справочники',
  referenceBook: 'Справочник',
};

export default textConstants;
