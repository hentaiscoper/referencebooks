import { AxiosResponse } from 'axios';
import axiosInstance from './apiConfig';

export default class Api {
  static getReferenceBooks(): Promise<AxiosResponse> {
    return axiosInstance.get('/referenceBooks');
  }
}
