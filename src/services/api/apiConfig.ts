import axios from 'axios';
import CommonUtils from '../../utils/commonUtils';
import RegularExpressions from '../../constants/regularExpressions';

const apiConstants = {
  timeout: 5000,
  apiPath: '',
};

export const Stands = {
  dev: 'http://localhost:5000/',
};

export const standUrl = Stands.dev;

const axiosInstance = axios.create({
  baseURL: standUrl + apiConstants.apiPath,
  timeout: apiConstants.timeout,
  headers: {
    'Content-Type': 'application/json',
  },
});

/*
 * Axios Interceptor для проверки правильности ввода эндпоинта api
 * */
axiosInstance.interceptors.request.use((config) => {
  const { url } = config;

  let validUrl = url || '';

  validUrl = CommonUtils.trimRepeatingCharacters(validUrl, RegularExpressions.url);

  return Promise.resolve({ ...config, url: validUrl });
});

export default axiosInstance;
