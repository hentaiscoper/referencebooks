import { Action } from 'redux';
import { Constants } from '../../types/constants';
import { Modal } from '../../types/store/modal';

const entity = 'MODAL';

export const ModalActionTypes = {
  OPEN_MODAL: `${entity}.OPEN_MODAL`,
  CLOSE_MODAL: `${entity}.CLOSE_MODAL`,
};

export default class ModalActions {
  static openModal = (modalType: Constants.ModalTypesKey): Modal.OpenModalAction => ({
    type: ModalActionTypes.OPEN_MODAL,
    payload: {
      modalType,
    },
  });

  static closeModal = (): Action<string> => ({
    type: ModalActionTypes.CLOSE_MODAL,
  });
}
