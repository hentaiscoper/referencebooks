import { Dispatch } from 'redux';
import { AxiosError, AxiosResponse } from 'axios';
import StoreUtils from '../../utils/storeUtils';
import Api from '../../services/api';
import { DataTypes } from '../../types/dataTypes';

const entity = 'REFERENCE_BOOKS';

export const ReferenceBooksActionTypes = StoreUtils.getGenericActionTypes(entity);

export default class ReferenceBooksActions {
  static getReferenceBooks = () => (dispatch: Dispatch): void => {
    dispatch({ type: ReferenceBooksActionTypes.FETCH_PENDING });

    Api.getReferenceBooks().then(
      (response: AxiosResponse<DataTypes.ReferenceBook[]>) => {
        dispatch({
          type: ReferenceBooksActionTypes.FETCH_SUCCESS,
          payload: response.data,
        });
      },
      (error: AxiosError) => {
        console.error(error);
        dispatch({ type: ReferenceBooksActionTypes.FETCH_FAILURE });
      }
    );
  };
}
