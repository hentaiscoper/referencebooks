import { combineReducers } from 'redux';
import referenceBooksReducer from './referenceBooksReducer';

const rootReducer = combineReducers({
  referenceBooks: referenceBooksReducer,
});

export default rootReducer;
