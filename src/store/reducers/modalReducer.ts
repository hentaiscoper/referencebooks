import { Action } from 'redux';
import { ModalConditions, ModalTypes } from '../../constants/modalConstants';
import { ModalActionTypes } from '../actions/modalActions';
import { Modal } from '../../types/store/modal';

const initialState: Modal.State = {
  modalCondition: ModalConditions.CLOSED,
  modalType: ModalTypes.NONE,
};

export default function modalReducer(
  state = initialState,
  action: Modal.OpenModalAction & Action<string>
): Modal.State {
  switch (action.type) {
    case ModalActionTypes.OPEN_MODAL:
      return {
        ...state,
        modalCondition: ModalConditions.OPEN,
        modalType: action.payload.modalType,
      };
    case ModalActionTypes.CLOSE_MODAL:
      return { ...state, modalCondition: ModalConditions.CLOSED };
    default:
      return { ...state };
  }
}
