import { ReferenceBooks } from '../../types/store/referenceBooks';
import FetchConstants from '../../constants/fetchConstants';
import { ReferenceBooksActionTypes } from '../actions/referenceBooksActions';

const initialState: ReferenceBooks.State = {
  data: [],
  fetchStatus: FetchConstants.WAITING,
};

export default function referenceBooksReducer(
  state = initialState,
  action: ReferenceBooks.ActionTypes
): ReferenceBooks.State {
  switch (action.type) {
    case ReferenceBooksActionTypes.FETCH_PENDING:
      return { ...state, fetchStatus: FetchConstants.PENDING };
    case ReferenceBooksActionTypes.FETCH_FAILURE:
      return { ...state, fetchStatus: FetchConstants.FAILURE };
    case ReferenceBooksActionTypes.FETCH_SUCCESS:
      return { ...state, fetchStatus: FetchConstants.SUCCESS, data: action.payload };
    default:
      return { ...state };
  }
}
