import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

let middlewares = applyMiddleware(thunk);

if (process.env.NODE_ENV !== 'production') {
  // eslint-disable-next-line @typescript-eslint/no-var-requires,global-require
  const composeWithDevtools = require('redux-devtools-extension').composeWithDevTools;

  middlewares = composeWithDevtools(middlewares);
}

const store = createStore(rootReducer, middlewares);

export default store;
