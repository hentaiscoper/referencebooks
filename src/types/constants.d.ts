import FetchConstants from '../constants/fetchConstants';
// eslint-disable-next-line import/named
import { ModalConditions, ModalTypes } from '../constants/modalConstants';

export declare module Constants {
  type FetchConstantsKey = keyof FetchConstants;

  type ModalConditionsKey = keyof ModalConditions;

  type ModalTypesKey = keyof ModalTypes;
}
