export declare module DataTypes {
  type Id = string | number;

  interface ReferenceBook {
    id: Id;
    title: string;
  }

  interface ReferenceBookGroup {
    id: Id;
    referenceBookId: Id;
    title: string;
  }

  interface GroupItem {
    id: Id;
    groupId: Id;
    title: string;
  }
}
