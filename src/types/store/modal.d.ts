import { Action } from 'redux';
import { Constants } from '../constants';

export declare module Modal {
  interface State {
    modalCondition: Constants.ModalConditionsKey;
    modalType: Constants.ModalTypesKey;
  }

  type ModalPayload = {
    modalType: Constants.ModalTypesKey;
  };

  interface OpenModalAction extends Action {
    payload: ModalPayload;
  }
}
