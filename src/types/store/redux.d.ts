import { Action, Store } from 'redux';
import rootReducer from '../../store/reducers';
import { Constants } from '../constants';

export declare module Redux {
  type RootState = ReturnType<typeof rootReducer>;

  type ThunkDispatch = ReduxThunkDispatch<RootState, unknown, Action>;

  type ThunkGetState = () => RootState;

  type ApplicationStore = Store<RootState>;

  interface GenericState<T> {
    data: T;
    fetchStatus: Constants.FetchConstantsKey;
  }

  interface GenericSaveStateAction<T> extends Action {
    payload: T;
  }

  interface GenericActionTypes {
    FETCH_PENDING: string;
    FETCH_SUCCESS: string;
    FETCH_FAILURE: string;
  }
}
