import { DataTypes } from '../dataTypes';
import { Redux } from './redux';

export declare module ReferenceBooks {
  type Data = DataTypes.ReferenceBook[];

  type State = Redux.GenericState<Data>;

  type SaveAction = Redux.GenericSaveStateAction<Data>;

  type ActionTypes = SaveAction;
}
