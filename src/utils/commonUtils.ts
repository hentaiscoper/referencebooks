export default class CommonUtils {
  static trimRepeatingCharacters(string: string, regularExp: RegExp): string {
    return string.replace(regularExp, (substring) => substring[0]);
  }
}
